# Changelog

## 2024-03-20
### Added
- Airspace (AIRAC AIP AMDT NR 250, 2023-09-07):
  - `RMZ Dubnica`
- Turnpoints:
  - `058DEMANDICE`
  - `107INOVEC`
  - `126KMETOVO`
  - `171MANKOVCE`
  - `203OSTRY GRUN`
  - `288VELKE LOVCE`
  - `293VINICA`
  - `301VYCAPY OPATOVCE`
  - `314ZLATNA NA OSTROVE`

### Changed
- Airspace (AIRAC AIP AMDT NR 250, 2023-09-07):
  -  Upper limit of `RMZ Prievidza` to 8000 ft MSL

## 2023-04-17
### Added
- Airspace:
  - `RMZ Jasna`
  - `RMZ Prievidza`
- Turnpoints:
  - `036CABAJ CAPOR PERES`
  - `287VITOVA`
  - `288VLCANY`

### Changed
- Airspace update from 2023-04-20:
  - `Out Contest Area Boundary` (due to TMA 3 Bratislava and hungarian border)
  - `LZTRA02 MTMA Malacky` renamed to `LZR2`
  - `LZTRA03 MTMA Malacky` renamed to `LZTRA3`
  - `LZTRA04A` renamed to `LZTRA4A`
  - `LZTRA05` renamed to `LZTRA5`
  - `LZTRA06A` renamed to `LZTRA6A`
  - `LZTRA06B` renamed to `LZTRA6B`
  - `LZTRA06C` renamed to `LZTRA6C`
  - `LZTRA09A` renamed to `LZTRA9A`
  - `LZTRA09W` renamed to `LZTRA9W`

### Removed
- Airspace out of contest area:
  - `LZP23 SALA`
  - Hungarian airspace
- Turnpoints outside contest area.

## 2023-04-08
### Added
- Turnpoints:
  - `007BABIA HORA`
  - `278TARJAN`
  - `308VOLOVEC`

### Changed
- Hungarian airspace.

## 2023-03-27
### Added
- Airspace:
  - `TRA225D`
  - `LZR241`
  - `Inovec`

### Removed
- Airspace:
  - `CTR SLIAC`
  - `TMA 1 SLIAC`
  - `TMA 2 SLIAC`
  - `TMA 3 SLIAC`
  - `TMA 4 SLIAC`

### Fixed
- Typo in turnpoint name `133KOMJATICE`.
- Frequencies in the airspace file.

## 2022-03-30
### Added
- Airspace update:
  - `Hungary G`
- Airspace update from 2021-12-30:
  - `ZUZANA`

### Changed
- Airspace update from 2021-12-30:
  - `LZR40`
  - `LZTRA04A`
  - `TMA 1 PIESTANY`
  - `TMA 4 BRATISLAVA`

### Fixed
- Airspace `Kosice TMA 4`.

## 2021-02-05
### Added
- Airspace update from 2020-12-03:
  - `Hornad`
  - `Kosice TMA 4`
  - `LZR415`
  - `LZR90A`
  - `LZR90B`
  - `LZR90C`
  - `LZTRA09A`
  - `LZTRA09W`
  - `LZTRA10`
  - `LZTRA100A`
  - `LZTRA100B`
  - `LZTRA100C`
- Turnpoints:
  - `197NOVA STRAZ`
  - `239ROZNAVA`

### Changed
- Update turnpoints descriptions.
- Airspace update from 2020-12-03:
  - `Boleraz 1`
  - `Boleraz 2`
  - `Chopok`
  - `CTA BRATISLAVA` (align due to Kosice TMA changes)
  - `Kosice TMA 3`
  - `LZR315`
  - `Out Contest Area Boundary` (align due to Kosice TMA changes)
  - `TMA 1 PIESTANY`
  - `TMA 2 PIESTANY`
  - `TMA 1 POPRAD`
  - `TMA 3 POPRAD`
  - `TMA 4 POPRAD`
  - `TMA 4 BRATISLAVA`

### Removed
- Airspace update from 2020-12-03:
  - `LZR90`
  - `LZR100A`
  - `LZTSA 09A`
  - `LZTSA 09W`
  - `LZTSA 10`

## 2019-04-10
### Changed
- Complete rework of the Airspace file in order to effectively define airspace which is relevant and minimize the file size.
- Sort turnpoints in such a way that their codes are assigned to their names alphabetically.
- Update airport frequencies.

### Removed
- Turnpoints outside contest area.
- Irrelevant airspace.

### Fixed
- Contest area boundary.
- Turnpoint codes.
- All known format and syntax issues.
