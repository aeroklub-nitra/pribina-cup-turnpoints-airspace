# Pribina Cup Turnpoints and Airspace

Turnpoints and Airspace files for Pribina Cup - International Gliding Competition, Nitra - Slovakia. These files are also suitable for other competitions held in Nitra airfield as well as for cross-country soaring in western part of Slovakia.

Files are designed:

- to effectively define airspace and points which are relevant and minimize the file size.
- in such a way that turnpoints codes are assigned to their names alphabetically.
- to adhere OpenAir and CUP file formats.
- to use Unix (LF) line separators.
- to be encoded in Unicode Standard UTF-8.

## Turnpoints

File: [pribina-cup-turnpoints.cup](pribina-cup-turnpoints.cup)

Format: [SeeYou CUP](http://download.naviter.com/docs/CUP-file-format-description.pdf)

## Airspace

File: [pribina-cup-airspace.txt](pribina-cup-airspace.txt)

Format: [OpenAir](http://www.winpilot.com/UsersGuide/UserAirspace.asp)

## Generate separate airspace files

Run script: `AirspaceFileSeparator.kts`

## Generate NDB file

1. Download software tools from [ILEC website](https://ilec-gmbh.com/software).
2. Convert SeeYou CUP file to NDB file using SeeYou (Save as).
3. Convert OpenAir file to NDB:
    1. Convert OpenAir file to TNP file. Use this [converter](https://notaminfo.com/php/openair-tnp.php).
    2. Run: `airPar32 pribina-cup-airspace.tnp "48:16.767 N 018:07.967 E"` (for more details see `mapHelp.txt`).
4. Append the airspace NDB file to the waypoints NDB file manually.
5. Verify the result file: `DBC32 pribina-cup_sua.ndb`.

Known issues:
- Class type "B" (OpenAir format) is converted as "AIRWAY" (TNP format) and `airPar32` reports: `Warning <Unrecognized airspace TYPE>`.

## Changelog

All notable changes are documented in the [CHANGELOG](CHANGELOG.md).

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
