import java.io.File

val folder = createFolder()
val sourceLines = readAirspaceFile()
var targetFileLines = mutableListOf<String>()
var targetFilename: String? = null

sourceLines.forEach { line ->
    if (line.startsWith("*")) {
        return@forEach
    }
    if (line.isBlank()) {
        return@forEach
    }
    if (line.startsWith("AC")) {
        savetoSeparatedFile()
        clearTargetAirspace()
    }
    if (line.startsWith("AN")) {
        assignTargetFilename(line)
    }
    targetFileLines.add(line)
}

savetoSeparatedFile()

fun createFolder(): File {
    val folder = File("separated_airspace_files")
    folder.mkdir()
    return folder
}

fun readAirspaceFile(): List<String> {
    return File("pribina-cup-airspace.txt").readLines()
}

fun assignTargetFilename(line: String) {
    val airspaceName = line.substring(3)
    targetFilename = airspaceName
        .lowercase()
        .replace(" ", "_")
        .replace(".", "_")
        .replace(",", "_")
        .replace("(", "")
        .replace(")", "") + ".txt"
}

fun savetoSeparatedFile() {
    if (targetFileLines.size == 0) {
        return
    }

    println("Saving to file: $targetFilename")

    File(folder, targetFilename).printWriter().use { printWriter ->
        targetFileLines.forEach { targetFileLine ->
            printWriter.println(targetFileLine)
        }
    }
}

fun clearTargetAirspace() {
    targetFileLines.clear()
    targetFilename = null
}
